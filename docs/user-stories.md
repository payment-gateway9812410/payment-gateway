# 1 - Process a payment

As a Merchant
I want to process a card payment
So that I can take payment from a buyer

## Test cases

GIVEN A buyer with valid card details
WHEN The merchant attempts to process a payment for the card
AND The buyer's bank allows retrieval of the payment
THEN The funds are debited from the buyer and credited to the merchant
AND The merchant receives a response indicating that the payment was successful

GIVEN A buyer with invalid card details
WHEN The merchant attempts to process a payment for the card
THEN The merchant receives a failure response indicating that the request could not be processed
AND The response contains details of why the response failed

GIVEN A buyer with valid card details
WHEN The merchant attempts to process a payment for the card
AND The buyer's bank does not allow retrieval of the payment
THEN The merchant receives a failure response indicating that the request could not be processed
AND The response contains details of why the response failed

GIVEN A buyer with valid card details
WHEN The merchant attempts to process a payment for the card
AND The buyer's bank does not allow retrieval of the payment due to fraud concerns
THEN The merchant receives a failure response indicating that the request could not be processed
AND The failure message is generic

## Assumptions

* For a failed payment, a merchant will want to know the reason why.
* In cases of fraud it may be necessary to not pass the reason for the payment failure onto the merchant.

# 2 - Retrieve historic payment

As a Merchant
I want to retrieve the details of a previously made payment
So that I can fulfil my reconciliation and reporting needs

## Test cases

GIVEN A payment has been made by a given merchant
WHEN The merchant attempts to retrieve the payment with its unique ID
THEN The merchant receives a response with details of the payment

GIVEN No payment for a given ID
WHEN The merchant attempts to retrieve the payment with the ID
THEN The merchant receives a failure response indicating that the payment was not found

GIVEN A payment has been made by a given merchant
WHEN Another merchant attempts to retrieve the payment with its unique ID
THEN The merchant receives a failure response indicating that the payment was not found

## Assumptions

* When a merchant attempts to access a payment they did not make themselves, they should receive a not found response rather than forbidden/unauthorised. This stops the system from leaking the fact that a certain payment has been made.