﻿namespace PaymentGatewayApi.Domain;

public class PaymentId
{
    public static PaymentId NewId() => new(Guid.NewGuid().ToString("N"));

    // This should perform validation.
    public static PaymentId FromString(string id) => new(id);

    public PaymentId(string id)
    {
        Id = id;
    }

    public string Id { get; set; }
    public override string ToString()
    {
        return Id;
    }

    public override bool Equals(object? obj)
    {
        if (obj == null)
        {
            return false;
        }

        if (obj.GetType() == GetType())
        {
            var id = (PaymentId)obj;
            return id.Id == Id;
        }

        return false;
    }
}
