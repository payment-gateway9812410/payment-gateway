﻿namespace PaymentGatewayApi.Domain;

public class PaymentStatusDetails
{
    public bool Success { get; set; }
    public string? FailureReason { get; set; }
}
