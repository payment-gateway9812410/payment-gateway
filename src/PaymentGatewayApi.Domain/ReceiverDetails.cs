﻿namespace PaymentGatewayApi.Domain;

public class ReceiverDetails
{
    public string AccountNumber { get; set; }
    public string SortCode { get; set; }
    public string Name { get; set; }
}
