﻿namespace PaymentGatewayApi.Domain;

public class SenderDetails
{
    public string Name { get; set; }
    public string CardNumber { get; set; }
    public string Expiry { get; set; }
    public string Cvv { get; set; }
    public string Currency { get; set; }
    public int Amount { get; set; }
}
