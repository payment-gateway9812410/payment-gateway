﻿namespace PaymentGatewayApi.Domain;

public class Payment
{
    // Required by entity framework.
    public Payment()
    {
    }
    public Payment(string senderName, string cardNumber, string expiry, string cvv, string currency, int amount, string accountNumber, string sortCode, string receiverName)
    {
        Sender = new SenderDetails
        {
            Name = senderName,
            Amount = amount,
            CardNumber = cardNumber,
            Expiry = expiry,
            Currency = currency,
            Cvv = cvv
        };

        Receiver = new ReceiverDetails
        {
            AccountNumber = accountNumber,
            Name = receiverName,
            SortCode = sortCode
        };

        Id = PaymentId.NewId();
    }

    public Payment(string id, SenderDetails sender, ReceiverDetails receiver, PaymentStatusDetails status)
    {
        Id = PaymentId.FromString(id);
        Sender = sender;
        Receiver = receiver;
        Status = status;
    }

    public void MarkPaymentSuccessful()
    {
        Status = new PaymentStatusDetails
        {
            Success = true
        };
    }

    public void MarkPaymentFailed(string? reason = null)
    {
        Status = new PaymentStatusDetails 
        {
            Success = false,
            FailureReason = reason
        };
    }

    public PaymentId Id { get; set; }
    public SenderDetails Sender { get; set; }
    public ReceiverDetails Receiver { get; set; }
    public PaymentStatusDetails? Status { get; private set; }
}
