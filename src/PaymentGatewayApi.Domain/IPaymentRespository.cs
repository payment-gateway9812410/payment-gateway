﻿namespace PaymentGatewayApi.Domain;

public interface IPaymentRespository
{
    Task Save(Payment payment);
    Task<Payment?> GetById(PaymentId Id);
}
