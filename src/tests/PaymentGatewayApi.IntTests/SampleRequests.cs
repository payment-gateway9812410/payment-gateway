﻿namespace PaymentGatewayApi.IntTests;

public static class SampleRequests
{
    public static readonly Contracts.ProcessPaymentRequest GoodRequest = new()
    {
        Sender = new Contracts.Sender
        {
            Name = "Joe Bloggs",
            CardNumber = "5247190106683106",
            Expiry = "12/2028",
            Amount = 10000,
            Currency = "GBP",
            Cvv = "360"
        },
        Receiver = new Contracts.Receiver
        {
            AccountNumber = "70328725",
            SortCode = "080054",
            Name = "Acme Widgets LTD"
        }
    };

    public static readonly Contracts.ProcessPaymentRequest BadRequest = new()
    {
        Sender = new Contracts.Sender
        {
            Name = "Joe Bloggs",
            CardNumber = "4485126628987868",
            Expiry = "11/2030",
            Amount = 50000,
            Currency = "GBP",
            Cvv = "938"
        },
        Receiver = new Contracts.Receiver
        {
            AccountNumber = "70328725",
            SortCode = "080054",
            Name = "Acme Widgets LTD"
        }
    };
}
