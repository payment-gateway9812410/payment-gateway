using WireMock.FluentAssertions;
using Microsoft.AspNetCore.Mvc.Testing;
using System.Net;
using System.Net.Http.Json;

namespace PaymentGatewayApi.IntTests;

public class ProcessPaymentTests : BaseApiTestFixture
{
    [Fact]
    public async Task ProcessPaymentWithValidRequest_ShouldReturnSuccess()
    {
        var application = new WebApplicationFactory<Program>();
        var client = application.CreateClient();

        var processPaymentRequest = SampleRequests.GoodRequest;

        var response = await client.PostAsJsonAsync("/payment", processPaymentRequest);

        Assert.True(response.IsSuccessStatusCode);
    }

    [Fact]
    public async Task ProcessPaymentWithInvalidRequest_ShouldNotReturnSuccess()
    {
        var application = new WebApplicationFactory<Program>();
        var client = application.CreateClient();

        var processPaymentRequest = SampleRequests.BadRequest;

        var response = await client.PostAsJsonAsync("/payment", processPaymentRequest);

        Assert.True(response.IsSuccessStatusCode);
        Assert.Equal(HttpStatusCode.Created, response.StatusCode);
    }

    [Fact]
    public async Task ProcessPaymentWithValidRequest_ShouldCallAcquiringBankFake()
    {
        var application = new WebApplicationFactory<Program>();
        var client = application.CreateClient();

        var processPaymentRequest = SampleRequests.GoodRequest;

        var response = await client.PostAsJsonAsync("/payment", processPaymentRequest);

        Assert.True(response.IsSuccessStatusCode);
        _wireMockServer.Should()
            .HaveReceivedACall()
            .AtUrl($"http://localhost:{AcquiringBankPort}/payment");
    }

    /// <summary>
    /// We expect the payments gateway to catch certain validation errors before calling the bank.
    /// </summary>
    [Fact]
    public async Task ProcessPaymentWithInValidRequest_ShouldNotCallAcquiringBankFake()
    {
        var application = new WebApplicationFactory<Program>();
        var client = application.CreateClient();

        // Request with invalid card number.
        var processPaymentRequest = new {
            Sender = new
            {
                Name = "Joe Bloggs",
                CardNumber = "notacardnumber",
                Expiry = "12/2028",
                Amount = 10000,
                Currency = "GBP",
                Cvv = "360"
            },
            Receiver = new
            {
                AccountNumber = "70328725",
                SortCode = "080054",
                Name = "Acme Widgets LTD"
            }
        }; ;

        var response = await client.PostAsJsonAsync("/payment", processPaymentRequest);

        Assert.False(response.IsSuccessStatusCode);
        _wireMockServer.Should()
            .HaveReceivedNoCalls()
            .AtUrl($"http://localhost:{AcquiringBankPort}/payment");
    }
}