﻿namespace PaymentGatewayApi.IntTests;

public class Contracts
{
    public class ProcessPaymentRequest
    {
        public Sender? Sender { get; set; }
        public Receiver? Receiver { get; set; }
    }

    public class ProcessPaymentResponse
    {
        public string? Id { get; set; }
        public Sender? Sender { get; set; }
        public Receiver? Receiver { get; set; }
    }

    public class Sender
    {
        public string? Name { get; set; }
        public string? CardNumber { get; set; }
        public string? Expiry { get; set; }
        public string? Cvv { get; set; }
        public string? Currency { get; set; }
        public int Amount { get; set; }
    }

    public class Receiver
    {
        public string? AccountNumber { get; set; }
        public string? SortCode { get; set; }
        public string? Name { get; set; }
    }
}
