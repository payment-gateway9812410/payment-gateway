﻿using AcquiringBankWireMock;
using Testcontainers.PostgreSql;
using WireMock.Server;

namespace PaymentGatewayApi.IntTests;

public class BaseApiTestFixture : IAsyncLifetime
{
    protected const int AcquiringBankPort = 9000;

    protected readonly WireMockServer _wireMockServer = WireMockServer.Start(AcquiringBankPort);

    protected readonly PostgreSqlContainer _postgreSqlContainer = new PostgreSqlBuilder().Build();

    public async Task InitializeAsync()
    {
        _wireMockServer.AddAcquiringBankRequests();
        Environment.SetEnvironmentVariable("AcquiringBankSettings__Url", $"http://localhost:{AcquiringBankPort}");

        await _postgreSqlContainer.StartAsync();
        Environment.SetEnvironmentVariable("ConnectionStrings__Database", _postgreSqlContainer.GetConnectionString());
    }

    public async Task DisposeAsync()
    {
        _wireMockServer.Stop();
        Environment.SetEnvironmentVariable("AcquiringBankSettings__Url", string.Empty);

        await _postgreSqlContainer.DisposeAsync();
        Environment.SetEnvironmentVariable("ConnectionStrings__Database", string.Empty);
    }
}
