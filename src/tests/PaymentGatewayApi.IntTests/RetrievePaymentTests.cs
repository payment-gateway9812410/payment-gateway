﻿using Microsoft.AspNetCore.Mvc.Testing;
using System.Net.Http.Json;

namespace PaymentGatewayApi.IntTests;

public class RetrievePaymentTests : BaseApiTestFixture
{
    [Fact]
    public async Task GivenSuccessfulPayment_PaymentShouldBeRetrievable()
    {
        var application = new WebApplicationFactory<Program>();
        var client = application.CreateClient();

        var processPaymentRequest = SampleRequests.GoodRequest;

        var response = await client.PostAsJsonAsync("/payment", processPaymentRequest);

        Assert.True(response.IsSuccessStatusCode);

        var location = response.Headers.Location;

        Assert.NotNull(location);

        var paymentResponse = await client.GetFromJsonAsync<Contracts.ProcessPaymentResponse>(location);

        Assert.NotNull(paymentResponse);
    }

    [Fact]
    public async Task GivenSuccessfulPayment_PaymentShouldBeRetrievableWithCorrectDetails()
    {
        var application = new WebApplicationFactory<Program>();
        var client = application.CreateClient();

        var processPaymentRequest = SampleRequests.GoodRequest;

        var response = await client.PostAsJsonAsync("/payment", processPaymentRequest);

        Assert.True(response.IsSuccessStatusCode);

        var location = response.Headers.Location;

        Assert.NotNull(location);

        var paymentResponse = await client.GetFromJsonAsync<Contracts.ProcessPaymentResponse>(location);

        Assert.NotNull(paymentResponse);
        Assert.Equal(processPaymentRequest?.Receiver?.Name, paymentResponse?.Receiver?.Name);
    }
}
