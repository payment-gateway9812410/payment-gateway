﻿using Microsoft.AspNetCore.Http.HttpResults;
using Moq;
using PaymentGatewayApi.Domain;
using PaymentGatewayApi.Features.RetrievePayment;

namespace PaymentGatewayApi.UnitTests.Features.RetrievePayment;

public class RetrievePaymentHandlerTests
{
    [Fact]
    public async Task GivenExistingPayment_ShouldRetrievePaymentById()
    {
        var mockId = "f007ae641f98428d8be653572ba38496";

        var payment = new Payment("Joe Bloggs", "5247190106683106", "12/2028", "360", "GBP", 10000, "70328725", "080054", "Acme Widgets LTD")
        {
            Id = PaymentId.FromString(mockId)
        };

        var mockRepository = new Mock<IPaymentRespository>();
        mockRepository.Setup(x => x.GetById(payment.Id)).ReturnsAsync(payment);

        var sut = new RetrievePaymentRequestHandler(mockRepository.Object);

        var request = new RetrievePaymentRequest { Id = mockId };

        var result = await sut.Handle(request, default);

        Assert.NotNull(result);
        Assert.IsType<Ok<RetrievePaymentResponse>>(result);

        var typedResult = (Ok<RetrievePaymentResponse>) result;

        Assert.Equal(payment.Receiver.Name, typedResult.Value?.Receiver.Name);
    }

    [Fact]
    public async Task GivenExistingPayment_CardNumberShouldBeMasked()
    {
        var mockId = "f007ae641f98428d8be653572ba38496";

        var payment = new Payment("Joe Bloggs", "5247190106683106", "12/2028", "360", "GBP", 10000, "70328725", "080054", "Acme Widgets LTD")
        {
            Id = PaymentId.FromString(mockId)
        };

        var mockRepository = new Mock<IPaymentRespository>();
        mockRepository.Setup(x => x.GetById(payment.Id)).ReturnsAsync(payment);

        var sut = new RetrievePaymentRequestHandler(mockRepository.Object);

        var request = new RetrievePaymentRequest { Id = mockId };

        var result = await sut.Handle(request, default);

        Assert.NotNull(result);
        Assert.IsType<Ok<RetrievePaymentResponse>>(result);

        var typedResult = (Ok<RetrievePaymentResponse>)result;

        Assert.Equal("************3106", typedResult.Value?.Sender.CardNumber);
    }

    [Fact]
    public async Task GivenExistingFailedPayment_FailureDetailsShouldBeAvailable()
    {
        var mockId = "f007ae641f98428d8be653572ba38496";

        var payment = new Payment("Joe Bloggs", "5247190106683106", "12/2028", "360", "GBP", 10000, "70328725", "080054", "Acme Widgets LTD")
        {
            Id = PaymentId.FromString(mockId)
        };

        payment.MarkPaymentFailed("Payment refused due to insufficient funds");

        var mockRepository = new Mock<IPaymentRespository>();
        mockRepository.Setup(x => x.GetById(payment.Id)).ReturnsAsync(payment);

        var sut = new RetrievePaymentRequestHandler(mockRepository.Object);

        var request = new RetrievePaymentRequest { Id = mockId };

        var result = await sut.Handle(request, default);

        Assert.NotNull(result);
        Assert.IsType<Ok<RetrievePaymentResponse>>(result);

        var typedResult = (Ok<RetrievePaymentResponse>)result;

        Assert.NotNull(typedResult.Value?.Status?.FailureReason);
        Assert.Equal(payment.Status?.FailureReason, typedResult.Value.Status.FailureReason);
    }
}
