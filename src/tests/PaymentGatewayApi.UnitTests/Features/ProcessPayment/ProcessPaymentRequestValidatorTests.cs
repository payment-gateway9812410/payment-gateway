﻿using FluentValidation.TestHelper;
using PaymentGatewayApi.Features.ProcessPayment;
using System.Runtime.InteropServices;

namespace PaymentGatewayApi.UnitTests.Features.ProcessPayment;

public class ProcessPaymentRequestValidatorTests
{
    private readonly ProcessPaymentRequestValidator _sut;

    public ProcessPaymentRequestValidatorTests()
    {
        _sut = new ProcessPaymentRequestValidator();
    }

    [Fact]
    public void GivenValidRquest_ShouldNotHaveError()
    {
        var request = new PaymentRequestBuilder()
           .Build();

        var result = _sut.TestValidate(request);

        result.ShouldNotHaveAnyValidationErrors();
    }

    [Fact]
    public void GivenInvalidCardNumber_ShouldHaveError()
    {
        var request = new PaymentRequestBuilder()
            .WithCardNumber("invalidcardnumber")
            .Build();

        var result = _sut.TestValidate(request);

        result.ShouldHaveValidationErrorFor(r => r.Sender.CardNumber);
    }

    [Fact]
    public void GivenInvalidCvv_ShouldHaveError()
    {
        var request = new PaymentRequestBuilder()
            .WithCvv("invalidcvv")
            .Build();

        var result = _sut.TestValidate(request);

        result.ShouldHaveValidationErrorFor(r => r.Sender.Cvv);
    }

    [Fact]
    public void GivenInvalidCurrency_ShouldHaveError()
    {
        var request = new PaymentRequestBuilder()
            .WithCurrency("pounds")
            .Build();

        var result = _sut.TestValidate(request);

        result.ShouldHaveValidationErrorFor(r => r.Sender.Currency);
    }
}
