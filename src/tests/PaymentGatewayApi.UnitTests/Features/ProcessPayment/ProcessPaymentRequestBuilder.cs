﻿using PaymentGatewayApi.Features.ProcessPayment;

namespace PaymentGatewayApi.UnitTests.Features.ProcessPayment;

public class PaymentRequestBuilder
{
    private readonly ProcessPaymentRequestCandidate _request;
    public PaymentRequestBuilder()
    {
        _request = new ProcessPaymentRequestCandidate
        {
            Sender = new()
            {
                Name = "Joe Bloggs",
                CardNumber = "5247190106683106",
                Expiry = "12/2028",
                Amount = 10000,
                Currency = "GBP",
                Cvv = "360"
            },
            Receiver = new()
            {
                AccountNumber = "70328725",
                SortCode = "080054",
                Name = "Acme Widgets LTD"
            }
        };
    }

    public ProcessPaymentRequest Build()
    {
        return new ProcessPaymentRequest
        {
            Sender = new()
            {
                Name = _request.Sender.Name,
                CardNumber = _request.Sender.CardNumber,
                Expiry = _request.Sender.Expiry,
                Amount = _request.Sender.Amount,
                Currency = _request.Sender.Currency,
                Cvv = _request.Sender.Cvv
            },
            Receiver = new()
            {
                AccountNumber = _request.Receiver.AccountNumber,
                SortCode = _request.Receiver.SortCode,
                Name = _request.Receiver.Name
            }
        };
    }

    public PaymentRequestBuilder WithCardNumber(string cardNumber)
    {
        _request.Sender.CardNumber = cardNumber;
        return this;
    }

    public PaymentRequestBuilder WithCvv(string cvv)
    {
        _request.Sender.Cvv = cvv;
        return this;
    }

    public PaymentRequestBuilder WithCurrency(string currency)
    {
        _request.Sender.Currency = currency;
        return this;
    }

    private class ProcessPaymentRequestCandidate
    {
        public required Sender Sender { get; set; }
        public required Receiver Receiver { get; set; }
    }

    private class Sender
    {
        public required string Name { get; set; }
        public required string CardNumber { get; set; }
        public required string Expiry { get; set; }
        public required string Cvv { get; set; }
        public required string Currency { get; set; }
        public required int Amount { get; set; }
    }

    private class Receiver
    {
        public required string AccountNumber { get; set; }
        public required string SortCode { get; set; }
        public required string Name { get; set; }
    }
}
