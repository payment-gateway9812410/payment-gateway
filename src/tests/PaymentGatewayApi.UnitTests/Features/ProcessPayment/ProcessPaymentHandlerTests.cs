﻿using AcquiringBankClient;
using AcquiringBankClient.Models;
using Microsoft.AspNetCore.Http.HttpResults;
using Moq;
using PaymentGatewayApi.Domain;
using PaymentGatewayApi.Features.ProcessPayment;

namespace PaymentGatewayApi.UnitTests.Features.ProcessPayment;

public class ProcessPaymentHandlerTests
{
    [Fact]
    public async Task GivenValidRequest_ShouldCallAcquiringBank()
    {
        var mockAcquiringBankClient = new Mock<IAcquiringBankClient>();
        mockAcquiringBankClient
            .Setup(x => x.ProcessPaymentAsync(It.IsAny<ProcessPaymentModel>()))
            .ReturnsAsync(new ProcessPaymentResponseModel { Success = true });

        var validator = new ProcessPaymentRequestValidator();

        var mockPaymentRespository = new Mock<IPaymentRespository>();
        mockPaymentRespository
            .Setup(x => x.Save(It.IsAny<Payment>()));

        var request = new PaymentRequestBuilder().Build();

        var sut = new ProcessPaymentHandler(mockAcquiringBankClient.Object, validator, mockPaymentRespository.Object);

        var result = await sut.Handle(request, default);

        Assert.IsType<Created>(result);
    }

    [Fact]
    public async Task GivenValidRequest_ShouldReturnLocationWithId()
    {
        var mockAcquiringBankClient = new Mock<IAcquiringBankClient>();
        mockAcquiringBankClient
            .Setup(x => x.ProcessPaymentAsync(It.IsAny<ProcessPaymentModel>()))
            .ReturnsAsync(new ProcessPaymentResponseModel { Success = true });

        var validator = new ProcessPaymentRequestValidator();

        var mockPaymentRespository = new Mock<IPaymentRespository>();
        mockPaymentRespository
            .Setup(x => x.Save(It.IsAny<Payment>()));

        var request = new PaymentRequestBuilder().Build();

        var sut = new ProcessPaymentHandler(mockAcquiringBankClient.Object, validator, mockPaymentRespository.Object);

        var result = await sut.Handle(request, default);

        Assert.IsType<Created>(result);

        var typedResult = (Created) result;

        Assert.False(string.IsNullOrEmpty(typedResult.Location));
    }

    [Fact]
    public async Task GivenUnsuccesfulPayment_ShouldStoreFailureReason()
    {
        var message = "Payment refused due to insufficient funds";
        var mockAcquiringBankClient = new Mock<IAcquiringBankClient>();
        mockAcquiringBankClient
            .Setup(x => x.ProcessPaymentAsync(It.IsAny<ProcessPaymentModel>()))
            .ReturnsAsync(new ProcessPaymentResponseModel { Success = false, Message = message });

        var validator = new ProcessPaymentRequestValidator();

        var savedPayment = new Payment();
        var mockPaymentRespository = new Mock<IPaymentRespository>();
        mockPaymentRespository
            .Setup(x => x.Save(It.IsAny<Payment>()))
            .Callback<Payment>(p => savedPayment = p);

        var request = new PaymentRequestBuilder().Build();

        var sut = new ProcessPaymentHandler(mockAcquiringBankClient.Object, validator, mockPaymentRespository.Object);

        var result = await sut.Handle(request, default);

        Assert.IsType<Created<string>>(result);

        Assert.NotNull(savedPayment);
        Assert.False(savedPayment.Status?.Success);
        Assert.Equal(message, savedPayment.Status?.FailureReason);
    }
}
