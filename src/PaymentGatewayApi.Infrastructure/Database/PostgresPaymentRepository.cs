﻿using Microsoft.EntityFrameworkCore;
using PaymentGatewayApi.Domain;

namespace PaymentGatewayApi.Infrastructure.Database;

internal class PostgresPaymentRepository : IPaymentRespository
{
    private readonly PaymentContext _paymentContext;

    public PostgresPaymentRepository(PaymentContext dbContext)
    {
        _paymentContext = dbContext;
    }

    public async Task<Payment?> GetById(PaymentId Id)
    {
        var payments = await _paymentContext.Payments.ToListAsync();
        var paymentEntity = await _paymentContext.Payments.AsNoTracking().FirstOrDefaultAsync(payment => payment.Id == Id.Id.ToString());
        return paymentEntity?.Payment;
    }

    public Task Save(Payment payment)
    {
        var paymentEntity = new PaymentEntity
        {
            Id = payment.Id.ToString(),
            Payment = payment
        };

        _paymentContext.Payments.Add(paymentEntity);
        return _paymentContext.SaveChangesAsync();
    }
}
