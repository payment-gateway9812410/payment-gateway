﻿using Microsoft.EntityFrameworkCore;

namespace PaymentGatewayApi.Infrastructure.Database;

internal class PaymentContext : DbContext
{
    public PaymentContext(DbContextOptions options) : base(options)
    {
    }

    public DbSet<PaymentEntity> Payments { get; set; }
}
