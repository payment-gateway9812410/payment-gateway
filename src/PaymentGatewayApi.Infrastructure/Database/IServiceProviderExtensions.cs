﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace PaymentGatewayApi.Infrastructure.Database;

public static class IServiceProviderExtensions
{
    public static IServiceProvider SetupDatabaseForDevelopment(this IServiceProvider serviceProvider)
    {
        using var scope = serviceProvider.CreateScope();
        var db = scope.ServiceProvider.GetRequiredService<PaymentContext>();
        db.Database.Migrate();
        return serviceProvider;
    }
}
