﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using PaymentGatewayApi.Domain;

namespace PaymentGatewayApi.Infrastructure.Database;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddPostgres(this IServiceCollection services, string connectionString)
    {
        services.AddDbContext<PaymentContext>(options => options.UseNpgsql(connectionString));
        services.AddTransient<IPaymentRespository, PostgresPaymentRepository>();
        return services;
    }
}
