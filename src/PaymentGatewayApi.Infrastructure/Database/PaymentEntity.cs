﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using PaymentGatewayApi.Domain;
using System.ComponentModel.DataAnnotations.Schema;

namespace PaymentGatewayApi.Infrastructure.Database;

internal class PaymentEntity
{
    public string Id { get; set; }

    [Column(TypeName = "jsonb")]
    public Payment Payment { get; set; }
}
