﻿using WireMock.Server;

namespace AcquiringBankWireMock
{
    public class WireMockService : IHostedService
    {
        private WireMockServer? _server;

        public Task StartAsync(CancellationToken cancellationToken)
        {
            var portConfig = Environment.GetEnvironmentVariable("WireMockPort");

            if (!int.TryParse(portConfig, out int port))
            {
                port = 8000;
            }

            _server = WireMockServer.Start(port);
            _server.AddAcquiringBankRequests();

            Console.WriteLine("Wire mock running on port/s {0}", string.Join(", ", _server.Ports));

            while (!cancellationToken.IsCancellationRequested)
            {
            }
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _server?.Stop();
            return Task.CompletedTask;
        }
    }
}
