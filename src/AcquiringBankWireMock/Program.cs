﻿using AcquiringBankWireMock;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddHostedService<WireMockService>();

var app = builder.Build();
app.Run();