﻿using System.Net;
using System.Text.Json;
using WireMock.Matchers;
using WireMock.RequestBuilders;
using WireMock.ResponseBuilders;
using WireMock.Server;

namespace AcquiringBankWireMock;

public static class WireMockExtensions
{
    public static WireMockServer AddAcquiringBankRequests(this WireMockServer server)
    {
        server
            .Given(Request.Create().WithPath("/payment").UsingPost().WithBody(
                new JsonMatcher(JsonSerializer.Serialize(Requests.GoodRequest), true)
            ))
            .RespondWith(Response.Create()
                .WithStatusCode(HttpStatusCode.OK));

        server
            .Given(Request.Create().WithPath("/payment").UsingPost().WithBody(
                new JsonMatcher(JsonSerializer.Serialize(Requests.InsufficientFundsRequest), true)
            ))
            .RespondWith(Response.Create()
                .WithStatusCode(HttpStatusCode.BadRequest)
                .WithBody("Payment refused due to insufficient funds"));

        server
            .Given(Request.Create().WithPath("/payment").UsingPost().WithBody(
                new JsonMatcher(JsonSerializer.Serialize(Requests.FraudulentRequest), true)
            ))
            .RespondWith(Response.Create()
                .WithStatusCode(HttpStatusCode.BadRequest)
                .WithBody("Payment refused due to fraud checks."));

        server
            .Given(Request.Create().WithPath("/payment").UsingPost().WithBody(
                new JsonMatcher(JsonSerializer.Serialize(Requests.InvalidReceiverRequest), true)
            ))
            .RespondWith(Response.Create()
                .WithStatusCode(HttpStatusCode.BadRequest)
                .WithBody("Payment could not be processed due to invalid receiver details."));

        server
            .Given(Request.Create().WithPath("/payment").UsingPost())
            .RespondWith(Response.Create()
                .WithStatusCode(HttpStatusCode.BadRequest));

        return server;
    }
}

public record SenderDetails(string CardNumber, string Expiry, string Cvv, string Currency, int Amount);
public record ReceiverDetails(string AccountNumber, string SortCode, string Name);
public record PaymentRequest(SenderDetails Sender, ReceiverDetails Receiver);

public static class Requests
{
    public static PaymentRequest GoodRequest = new(
        new SenderDetails("5247190106683106", "12/2028", "360", "GBP", 10000),
        new ReceiverDetails("70328725", "080054", "Acme Widgets LTD")
    );

    public static PaymentRequest InsufficientFundsRequest = new(
        new SenderDetails("4485126628987868", "11/2030", "938", "GBP", 50000),
        new ReceiverDetails("70328725", "080054", "Acme Widgets LTD")
    );

    public static PaymentRequest FraudulentRequest = new(
        new SenderDetails("3475265008472600", "2/2024", "973", "GBP", 75000),
        new ReceiverDetails("70328725", "080054", "Acme Widgets LTD")
    );

    public static PaymentRequest InvalidReceiverRequest = new(
        new SenderDetails("5247190106683106", "12/2028", "360", "GBP", 10000),
        new ReceiverDetails("70872491", "404784", "Bob's Burgers")
    );
}