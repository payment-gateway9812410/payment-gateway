﻿using MediatR;

namespace PaymentGatewayApi.Features.RetrievePayment;

public class RetrievePaymentRequest : IRequest<IResult>
{
    public required string Id { get; set; }
}
