﻿using PaymentGatewayApi.Features.ProcessPayment;

namespace PaymentGatewayApi.Features.RetrievePayment;

public class RetrievePaymentResponse
{
    public required string PaymentId { get; init; }
    public required Sender Sender { get; init; }
    public required Receiver Receiver { get; init; }
    public PaymentStatus? Status { get; set; }
}
