﻿using MediatR;
using PaymentGatewayApi.Domain;
using PaymentGatewayApi.Features.ProcessPayment;

namespace PaymentGatewayApi.Features.RetrievePayment;

public class RetrievePaymentRequestHandler : IRequestHandler<RetrievePaymentRequest, IResult>
{
    private readonly IPaymentRespository _paymentRespository;

    public RetrievePaymentRequestHandler(IPaymentRespository paymentRespository)
    {
        _paymentRespository = paymentRespository;
    }

    public async Task<IResult> Handle(RetrievePaymentRequest request, CancellationToken cancellationToken)
    {
        // Remove any pesky whitespace from the id.
        var id = request.Id.Trim();

        var payment = await _paymentRespository.GetById(PaymentId.FromString(id));

        if (payment == null)
        {
            return TypedResults.NotFound();
        }

        var response = MaskDetails(payment);

        return TypedResults.Ok(response);
    }

    private static RetrievePaymentResponse MaskDetails(Payment payment)
    {
        // Assumes that card numbers have a fixed length.
        var lastFourDigits = payment.Sender.CardNumber[^4..];
        var maskedCardNumber = $"************{lastFourDigits}";

        var paymentResponse = new RetrievePaymentResponse
        {
            PaymentId = payment.Id.ToString(),
            Sender = new Sender
            {
                Name = payment.Sender.Name,
                Amount = payment.Sender.Amount,
                CardNumber = maskedCardNumber,
                Currency = payment.Sender.Currency,
                Cvv = payment.Sender.Cvv,
                Expiry = payment.Sender.Expiry
            },
            Receiver = new Receiver
            {
                AccountNumber = payment.Receiver.AccountNumber,
                Name = payment.Receiver.Name,
                SortCode = payment.Receiver.SortCode
            }
        };

        if (payment.Status != null)
        {
            paymentResponse.Status = new PaymentStatus
            {
                Success = payment.Status.Success,
                FailureReason = payment.Status.FailureReason
            };
        }

        return paymentResponse;
    }
}
