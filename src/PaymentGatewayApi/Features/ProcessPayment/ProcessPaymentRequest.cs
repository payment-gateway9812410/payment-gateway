﻿using MediatR;

namespace PaymentGatewayApi.Features.ProcessPayment;

public class ProcessPaymentRequest : IRequest<IResult>
{
    public required Sender Sender { get; init; }
    public required Receiver Receiver { get; init; }
}

public class Sender
{
    public required string Name { get; init; }
    public required string CardNumber { get; init; }
    public required string Expiry { get; init; }
    public required string Cvv { get; init; }
    public required string Currency { get; init; }
    public required int Amount { get; init; }
}

public class Receiver
{
    public required string AccountNumber { get; init; }
    public required string SortCode { get; init; }
    public required string Name { get; init; }
}

public class PaymentStatus
{
    public required bool Success { get; set; }
    public string? FailureReason { get; set; }
}
