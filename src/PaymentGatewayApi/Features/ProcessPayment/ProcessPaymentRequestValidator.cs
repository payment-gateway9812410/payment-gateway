﻿using FluentValidation;

namespace PaymentGatewayApi.Features.ProcessPayment;

public class ProcessPaymentRequestValidator : AbstractValidator<ProcessPaymentRequest>
{
    public ProcessPaymentRequestValidator()
    {
        RuleFor(request => request.Sender.CardNumber).Matches("\\d{4}\\s?\\d{4}\\s?\\d{4}\\s?\\d{4}\\s?");
        RuleFor(request => request.Sender.Cvv).Matches("\\d{3}");
        RuleFor(request => request.Sender.Currency).Matches("[A-Z]{3}");
    }
}
