﻿using AcquiringBankClient;
using AcquiringBankClient.Models;
using FluentValidation;
using MediatR;
using PaymentGatewayApi.Domain;

namespace PaymentGatewayApi.Features.ProcessPayment;

public class ProcessPaymentHandler : IRequestHandler<ProcessPaymentRequest, IResult>
{
    private readonly IAcquiringBankClient _acquiringBankClient;
    private readonly IValidator<ProcessPaymentRequest> _validator;
    private readonly IPaymentRespository _paymentRespository;

    public ProcessPaymentHandler(IAcquiringBankClient acquiringBankClient, IValidator<ProcessPaymentRequest> validator, IPaymentRespository paymentRespository)
    {
        _acquiringBankClient = acquiringBankClient;
        _validator = validator;
        _paymentRespository = paymentRespository;
    }

    public async Task<IResult> Handle(ProcessPaymentRequest request, CancellationToken cancellationToken)
    {
        // Validate request.
        var validationResult = _validator.Validate(request);

        if (validationResult.Errors.Any())
        {
            return TypedResults.BadRequest(validationResult.Errors);
        }

        var payment = new Payment(
            request.Sender.Name,
            request.Sender.CardNumber,
            request.Sender.Expiry,
            request.Sender.Cvv,
            request.Sender.Currency,
            request.Sender.Amount,
            request.Receiver.AccountNumber,
            request.Receiver.SortCode,
            request.Receiver.Name
            );

        return await ProcessPayment(payment);
    }

    private async Task<IResult> ProcessPayment(Payment payment)
    {
        var processPaymentModel = ProcessPaymentModel.FromPayment(payment);

        var response = await _acquiringBankClient.ProcessPaymentAsync(processPaymentModel);

        if (!response.Success)
        {
            payment.MarkPaymentFailed(response.Message);
            await _paymentRespository.Save(payment);
            return TypedResults.Created($"/payment/{payment.Id}", response.Message);
        }

        payment.MarkPaymentSuccessful();
        await _paymentRespository.Save(payment);

        return TypedResults.Created($"/payment/{payment.Id}");
    }
}
