using AcquiringBankClient;
using AcquiringBankClient.Configuration;
using FluentValidation;
using MediatR;
using PaymentGatewayApi;
using PaymentGatewayApi.Features.ProcessPayment;
using PaymentGatewayApi.Features.RetrievePayment;
using PaymentGatewayApi.Infrastructure.Database;
using Swashbuckle.AspNetCore.Filters;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

builder.Services
    .AddLogging(builder =>
    {
        builder.AddJsonConsole();
    });

builder.Services
    .AddOptions<AcquiringBankSettings>()
    .Bind(builder.Configuration.GetSection(AcquiringBankSettings.Key));

builder.Services.AddTransient<IAcquiringBankClient, AcquiringBankClient.AcquiringBankClient>();
builder.Services.AddHttpClient<IAcquiringBankClient, AcquiringBankClient.AcquiringBankClient>();

builder.Services.AddValidatorsFromAssemblyContaining<ProcessPaymentRequestValidator>();

builder.Services.AddMediatR(c =>
{
    c.Lifetime = ServiceLifetime.Scoped;
    c.RegisterServicesFromAssembly(typeof(Program).Assembly);
});

var connectionString = builder.Configuration.GetConnectionString("database");
builder.Services.AddPostgres(connectionString);

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.ExampleFilters();
});
builder.Services.AddSwaggerExamplesFromAssemblies(Assembly.GetEntryAssembly());

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();

    app.Services.SetupDatabaseForDevelopment();
}

app.UseHttpsRedirection();

app.MapPost("payment", [SwaggerRequestExample(typeof(ProcessPaymentRequest), typeof(Examples))] async (IMediator mediator, ProcessPaymentRequest request) => await mediator.Send(request));
app.MapGet("payment/{id}", async (IMediator mediator, string id) => await mediator.Send(new RetrievePaymentRequest { Id = id }));

app.Run();
