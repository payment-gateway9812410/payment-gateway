﻿using PaymentGatewayApi.Features.ProcessPayment;
using Swashbuckle.AspNetCore.Filters;

namespace PaymentGatewayApi;

public class Examples : IMultipleExamplesProvider<ProcessPaymentRequest>
{
    public IEnumerable<SwaggerExample<ProcessPaymentRequest>> GetExamples()
    {
        yield return SwaggerExample.Create(nameof(Requests.GoodRequest), Requests.GoodRequest);
        yield return SwaggerExample.Create(nameof(Requests.InsufficientFundsRequest), Requests.InsufficientFundsRequest);
        yield return SwaggerExample.Create(nameof(Requests.FraudulentRequest), Requests.FraudulentRequest);
        yield return SwaggerExample.Create(nameof(Requests.InvalidReceiverRequest), Requests.InvalidReceiverRequest);
    }
}
public static class Requests
{
    public static ProcessPaymentRequest GoodRequest = new()
    {
        Sender = new Sender
        {
            Name = "Joe Bloggs",
            CardNumber = "5247190106683106",
            Expiry = "12/2028",
            Cvv = "360",
            Currency = "GBP",
            Amount = 10000
        },
        Receiver = new Receiver
        {
            AccountNumber = "70328725",
            SortCode = "080054",
            Name = "Acme Widgets LTD"
        }
    };
    public static ProcessPaymentRequest InsufficientFundsRequest = new()
    {
        Sender = new Sender
        {
            Name = "Joe Bloggs",
            CardNumber = "4485126628987868",
            Expiry = "11/2030",
            Cvv = "938",
            Currency = "GBP",
            Amount = 50000
        },
        Receiver = new Receiver
        {
            AccountNumber = "70328725",
            SortCode = "080054",
            Name = "Acme Widgets LTD"
        }
    };

    public static ProcessPaymentRequest FraudulentRequest = new()
    {
        Sender = new Sender
        {
            Name = "Joe Bloggs",
            CardNumber = "3475265008472600",
            Expiry = "2/2024",
            Cvv = "973",
            Currency = "GBP",
            Amount = 75000
        },
        Receiver = new Receiver
        {
            AccountNumber = "70328725",
            SortCode = "080054",
            Name = "Acme Widgets LTD"
        }
    };

    public static ProcessPaymentRequest InvalidReceiverRequest = new()
    {
        Sender = new Sender
        {
            Name = "Joe Bloggs",
            CardNumber = "5247190106683106",
            Expiry = "12/2028",
            Cvv = "360",
            Currency = "GBP",
            Amount = 10000
        },
        Receiver = new Receiver
        {
            AccountNumber = "70872491",
            SortCode = "404784",
            Name = "Bob's Burgers"
        }
    };
}