namespace PaymentGatewayApi.Domain.UnitTests;

public class PaymentTests
{
    [Fact]
    public void GivenValidPaymentDetails_CanCreatePayment()
    {
        var pay = new
        {
            Sender = new
            {
                Name = "Joe Bloggs",
                CardNumber = "5247190106683106",
                Expiry = "12/2028",
                Amount = 10000,
                Currency = "GBP",
                Cvv = "360"
            },
            Receiver = new
            {
                AccountNumber = "70328725",
                SortCode = "080054",
                Name = "Acme Widgets LTD"
            }
        };

        var payment = new Payment(pay.Sender.Name, pay.Sender.CardNumber, pay.Sender.Expiry, pay.Sender.Cvv, pay.Sender.Currency, pay.Sender.Amount, pay.Receiver.AccountNumber, pay.Receiver.SortCode, pay.Receiver.Name);

        Assert.NotNull(payment);
    }
}