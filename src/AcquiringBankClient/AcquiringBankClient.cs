﻿using AcquiringBankClient.Configuration;
using AcquiringBankClient.Models;
using Microsoft.Extensions.Options;
using System.Net.Http.Json;

namespace AcquiringBankClient;

public class AcquiringBankClient : IAcquiringBankClient
{
    private readonly HttpClient _httpClient;

    public AcquiringBankClient(IOptions<AcquiringBankSettings> settings, HttpClient httpClient)
    {
        _httpClient = httpClient;
        _httpClient.BaseAddress = settings.Value.Url;
    }

    public async Task<ProcessPaymentResponseModel> ProcessPaymentAsync(ProcessPaymentModel payment)
    {
        var response = await _httpClient.PostAsJsonAsync("/payment", payment);

        if (response.IsSuccessStatusCode)
        {
            return new ProcessPaymentResponseModel
            {
                Success = true
            };
        }

        return new ProcessPaymentResponseModel
        {
            Success = false,
            Message = await response.Content.ReadAsStringAsync()
        };
    }
}
