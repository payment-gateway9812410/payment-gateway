﻿namespace AcquiringBankClient.Configuration;

public class AcquiringBankSettings
{
    public const string Key = nameof(AcquiringBankSettings);
    public required Uri Url { get; init; }
}
