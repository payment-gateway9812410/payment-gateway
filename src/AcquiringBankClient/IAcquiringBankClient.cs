﻿using AcquiringBankClient.Models;

namespace AcquiringBankClient;

public interface IAcquiringBankClient
{
    Task<ProcessPaymentResponseModel> ProcessPaymentAsync(ProcessPaymentModel payment);
}
