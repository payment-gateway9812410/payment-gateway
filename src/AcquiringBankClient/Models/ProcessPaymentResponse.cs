﻿namespace AcquiringBankClient.Models;

public class ProcessPaymentResponseModel
{
    public required bool Success { get; init; }
    public string? Message { get; set; }
}
