﻿namespace AcquiringBankClient.Models;

public class SenderModel
{
    public required string CardNumber { get; init; }
    public required string Expiry { get; init; }
    public required string Cvv { get; init; }
    public required string Currency { get; init; }
    public required int Amount { get; init; }
}
