﻿namespace AcquiringBankClient.Models;

public class ReceiverModel
{
    public required string AccountNumber { get; init; }
    public required string SortCode { get; init; }
    public required string Name { get; init; }
}