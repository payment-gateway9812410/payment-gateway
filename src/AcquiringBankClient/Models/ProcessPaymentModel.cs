﻿using PaymentGatewayApi.Domain;

namespace AcquiringBankClient.Models;

public class ProcessPaymentModel
{
    public static ProcessPaymentModel FromPayment(Payment payment)
    {
        return new ProcessPaymentModel
        {
            Sender = new SenderModel
            {
                Amount = payment.Sender.Amount,
                CardNumber = payment.Sender.CardNumber.ToString(),
                Currency = payment.Sender.Currency,
                Cvv = payment.Sender.Cvv,
                Expiry = payment.Sender.Expiry.ToString()
            },
            Receiver = new ReceiverModel
            {
                AccountNumber = payment.Receiver.AccountNumber,
                Name = payment.Receiver.Name,
                SortCode = payment.Receiver.SortCode
            }
        };
    }

    public required SenderModel Sender { get; init; }
    public required ReceiverModel Receiver { get; init; }
}
