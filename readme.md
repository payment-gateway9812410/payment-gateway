# Payment Gateway

## Running the solution

### Requirements

* Docker/docker-compose
* dotnet CLI

To trust created a trusted certificate for the locally hosted application, run the following commands:

#### Powershell
```
dotnet dev-certs https -ep $env:UserProfile\.aspnet\https\aspnetapp.pfx -p Myc3rp455word!
dotnet dev-certs https --trust
```

#### Bash
```
dotnet dev-certs https -ep ~/.aspnet/https/aspnetapp.pfx -p Myc3rp455word!
dotnet dev-certs https --trust
```

and when prompted, select "yes" to trust the certificate.

From the `src` folder run the following command to build the solution:

`docker-compose  -f ".\docker\docker-compose.yml" -f ".\docker\docker-compose.override.yml" build`

Then to run:

`docker-compose  -f ".\docker\docker-compose.yml" -f ".\docker\docker-compose.override.yml" up`

Navigate to https://localhost:8080/swagger/index.html in your browser and trust the certificate when prompted.

#### Creating a payment

Some sample POST requests for creating payments have been bundled with the swaggerdoc:

![POST requests in swagger](/docs/swagger-1.png?raw=true)

#### Retrieving a payment

Once a payment has been created, it can be retrieved by pasting the `id` component of the location header into into the GET endpoint:

![GET requests in swagger](/docs/swagger-2.png?raw=true)

## Running the tests

### Requirements

* dotnet 7.0 sdk
* Docker

### Powershell

In the src directory, run:

`.\tests\Run-Unit-Tests.ps1` to run the unit tests, or
`.\tests\Run-Integration-Tests.ps1` to run the integration tests.

### dotnet test

In your terminal of choice, run:

`dotnet test --filter "FullyQualifiedName~UnitTests"` to run the unit tests, or
`dotnet test --filter "FullyQualifiedName~IntTests"` to run the integration tests.

## Technology breakdown

The PaymentGatewayAPI is a dotnet minimal API using Mediatr to marshall request logic and provide a neat hook for unit testing.

The current persistence mechanism is PostgreSql using EF Core as an ORM, storing the Payments as jsonb columns. The PostgreSql-specific logic is contained within the PaymentGatewayApi.Infrastructure project, so could be replaced with another persistence mechanism that can implement the IPaymentRepository interface in the Domain project. The jsonb type has been used in order to make a transition to other nosql data stores (MongoDB, DynamoDB etc.) relatively easy.

The unit tests are written with XUnit and Moq, as is standard for this dotnet projects.

The integration tests use the WebApplicationFactory to create an in-memory representation of the API along with WireMock.Net as the mechanism for simulating the AcquiringBank and TestContainers.Net to spin up a Postgres container.

## Necessary Improvements

In order to make this system "production ready" several improvements would have to be made:

### Increased validation

The system currently doesn't perform all the required validation that is possible. Additional logic should be built into the `ProcessPaymentRequestValidator` as well as the `Payment` domain object to validate things like amounts, card numbers, cvv's and so on.

Certain aspects of this logic could be separated into a separate, content-driven service in order to allow it to be managed by business people.

### Authentication/Authorisation

The system doesn't perform any checks to verify the identity or permissions of the user.

A tenancy model could be created to associate all payments with a particular merchant, and specific users created under each tenant with finer grained permissions. IE the ability to make payments, the ability to retrieve historical payments etc. As well as providing the possibility for rate-limited various tiers of customer.

### Security hardening

An example of security hardening that should take place could be to create a reverse proxy to handle TLS termination and set various security-specific headers, for example [those recommended by OWASP](https://cheatsheetseries.owasp.org/cheatsheets/REST_Security_Cheat_Sheet.html).

### Increased testing

Certain paths have not been covered with unit or integration testing. These tests should be written before such a system goes live.

## Potential Improvements

### Cloud technologies

This solution has been built without any cloud technologies but nothing precludes this. The simplest way to deploy it would be a combination of some kind of container orchestration (Amazon ECS, Kubernetes) and a Postges database (Amazon RDS). However it would be trivial to create a persistence mechanism using DynamoDB and deploy the API as a Lambda to embrace a cloud-native approach.

### Messaging

Currently the solution does not handle commands and queries in fundamentally different ways, they both use the same domain model and synchronous calls to the database. A possible improvement could be to publish an event for each payment created and have it picked up by a message handler running separately. This would allow the two pieces of functionality to scale separately.

The solution also only allows communication via its REST API. If it were to publish events it could allow other systems to subscribe to them and take action as necessary.